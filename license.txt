[adblock plus 3.0]
! title: purify web
! homepage: https://github.com/jubyshu/purifyweb
! licence: https://github.com/jubyshu/purifyweb/blob/master/license.txt
! expires: 7 days
! version: 2019.10.05

! https://www.instagram.com
www.instagram.com##.ZUqME.b2rUF._49XvD._22l1._4EzTm.eGOV_.IwRSH.Igw0E.DPiy6._6Rvw2
www.instagram.com##.Xt-CQ

! https://www.douban.com
||ad.doubanio.com/$subdocument
www.douban.com###dale_subject_search_top_right > div
! https://movie.douban.com
movie.douban.com###dale_movie_subject_middle_right > div
movie.douban.com###dale_movie_subject_top_right > div
movie.douban.com###dale_movie_subject_bottom_super_banner > div
! https://book.douban.com
book.douban.com###dale_book_subject_top_right > div
book.douban.com###dale_book_subject_middle_right > div
book.douban.com###dale_book_subject_bottom_super_banner > div
! https://music.douban.com
music.douban.com###dale_music_home_top_right > div
music.douban.com###dale_music_subject_top_right > div
music.douban.com###dale_music_subject_middle_right > div
! 9/22/2019 https://www.douban.com
www.douban.com###dale_group_topic_new_top_right
www.douban.com###dale_group_topic_new_bottom_right
www.douban.com###dale_group_topic_bottom_super_banner

! https://bbs.hupu.com
bbs.hupu.com##.shihuo_photo_news
! 9/22/2019 https://m.hupu.com
m.hupu.com##.m-app
m.hupu.com###J_live_container

! https://www.economist.com
www.economist.com##.ribbon__banner
www.economist.com##.ribbon__body
www.economist.com##.ribbon__header
www.economist.com##.newsletter-form--inline.newsletter-form
www.economist.com##.end-of-article-grey-template__body

! 9/20/2019 https://www.newyorker.com
www.newyorker.com##.GrowlerFailsafe__expanded___e0wfF.GrowlerFailsafe__growlerWrapper___24tCW
||www.newyorker.com/images/CM/failsafe/cns/NYR_Rightrail_Sept18_Failsafe*.png$image
||www.newyorker.com/images/CM/failsafe/cns/NYR_Footer_Sept18_Failsafe*.png$image
||www.newyorker.com/images/CM/failsafe/cns/NYR_Contentriver_Sept18_Failsafe*.png$image
||www.newyorker.com/images/CM/failsafe/cns/NYR_Header_Sept18_Failsafe*.png$image
www.newyorker.com##.Paywall__show___yW09h.Paywall__barrier___1LkDX

! 9/20/2019 https://medium.com
medium.com###mediumUnlimited
###mediumUnlimited

! 9/20/2019 https://www.zhibo8.cc
www.zhibo8.cc##.margin_top_20.container
www.zhibo8.cc##.right.bbs_container
! 9/22/2019 https://www.zhihu.com
www.zhihu.com###special
! 9/22/2019 https://www.zhihu.com
www.zhihu.com##div.Card:nth-of-type(2)
www.zhihu.com##div.Card:nth-of-type(5)

! 9/20/2019 https://www.zhihu.com
www.zhihu.com##.AppBanner-layout
www.zhihu.com##.Question-sideColumnAdContainer
! 9/22/2019 https://www.zhihu.com
www.zhihu.com##.is-shown.OpenInApp.OpenInAppButton

! 9/21/2019 https://sm.ms
sm.ms##.detail-advert
sm.ms##.advert_foot.col-lg-6

! 9/21/2019 https://www.douyu.com
www.douyu.com##.is-show.ActBase.ActSuperFansGroup

! 9/22/2019 https://sports.qq.com
sports.qq.com##.play_fix.clearfix.videocont

! 9/22/2019 https://v2ex.com
v2ex.com###Rightbar > div.box:nth-of-type(4)
v2ex.com###Rightbar > div.box:nth-of-type(6)

! 9/22/2019 https://www.nytimes.com
www.nytimes.com##.css-zh9j4b
www.nytimes.com##.egw6tc40.css-b5hohh
www.nytimes.com##.ejw0p350.css-9ghvn0
www.nytimes.com##.e1x0szx60.css-16r57cp.standard.shown.meter_default
www.nytimes.com##.css-1k9ek97

! 9/22/2019 https://cn.nytimes.com
cn.nytimes.com##.ribbon-show.article-ribbon
cn.nytimes.com###subscribe_cont
cn.nytimes.com###subscribe_mobile_cont

! 9/22/2019 https://www.wsj.com
www.wsj.com###cx-candybar-wrapper
www.wsj.com##.style--secure-drop-tips--lOKwoM1i
www.wsj.com###cx-articlecover
www.wsj.com##.article__inset--wrap.article__inset--type-InsetRichText.article__inset.scope-web.wrap.type-InsetRichText.media-object

! 9/22/2019 https://www.meiju.net
www.meiju.net###layui-layer1

! 9/22/2019 https://www.vanityfair.com
www.vanityfair.com##.paywall-bar__expanded
www.vanityfair.com##.persistent-bottom

! 9/22/2019 https://www.bloomberg.com
www.bloomberg.com###paywall-banner
www.bloomberg.com##.open-zipr.zipr-recirc__under-navi--desktop.zipr-recirc

! 9/22/2019 https://www.caixinglobal.com
www.caixinglobal.com##.down-app-box

! 9/22/2019 https://www.wired.com
www.wired.com##.persistent-bottom
www.wired.com##.display-hero-failsafe
www.wired.com##div.row:nth-of-type(6)

! 9/22/2019 https://m.hupu.com
m.hupu.com##.open-app

! 9/23/2019 https://www.reddit.com
www.reddit.com##.TopButton
www.reddit.com##.XPromoPill
www.reddit.com##li.OverlayMenu-row:nth-of-type(8) > .OverlayMenu-row-button

! 9/23/2019 https://m.facebook.com
m.facebook.com###login_top_banner
m.facebook.com##._3on5._55wo

! 9/23/2019 https://music.163.com
music.163.com##.topfr
music.163.com##.openapp

! 9/23/2019 https://m.bilibili.com
m.bilibili.com##.index__container__src-commonComponent-bottomOpenApp-
m.bilibili.com##.index__openAppBtn__src-commonComponent-topArea-

! 9/23/2019 https://m.douyu.com
m.douyu.com##.HomeFooter-downLoadClient
m.douyu.com##.HomeHeader-openAppB

! 9/23/2019 https://www.douyu.com
www.douyu.com##.is-show.AppFlow
www.douyu.com###js-aside
www.douyu.com##.is-show.ActBase.Act159742
www.douyu.com##.Bottom-ad
www.douyu.com###guess-main-panel
www.douyu.com###js-bottom
www.douyu.com##.Title-roomOtherBottom
www.douyu.com##.PlayerToolbar-reactGroup
www.douyu.com##.ToolbarActivityArea
www.douyu.com##.ToolbarGiftArea
www.douyu.com##.PlayerToolbar-signCont
www.douyu.com##.Header-download-wrap
www.douyu.com##li.Header-menu-link:nth-of-type(5)
www.douyu.com##li.Header-menu-link:nth-of-type(6)
www.douyu.com###js-main
www.douyu.com##.is-show.Elevator
www.douyu.com###js-footer
www.douyu.com###js-right-nav
www.douyu.com##.is-active.UPlayerLotteryEnter
www.douyu.com##.Title-videoSiteLink
www.douyu.com##.LotteryContainer
www.douyu.com##.HeaderNav
www.douyu.com##.is-normalRoom.guessGameContainer
! 9/29/2019 https://www.douyu.com
www.douyu.com##.MatchSystemGuide
! 9/30/2019 https://www.douyu.com
www.douyu.com##.FirePowerChatModal-Notice
! 10/2/2019 https://www.douyu.com
www.douyu.com###bc4 > .wm-h5-view
www.douyu.com##.ad-box-f661ba
! 9/27/2019 https://www.douyu.com
www.douyu.com##.Barrage-chat-ad.RoomChat.SignBaseComponent-sign-box
! 9/24/2019 https://www.douyu.com
www.douyu.com##.recommendApp-0e23eb
www.douyu.com##.recommendAD-54569e

! 9/23/2019 https://www.bilibili.com
www.bilibili.com###fixed_app_download
www.bilibili.com##.clearfix.live-module > .gg-floor-module
www.bilibili.com##.mobile.nav-item
! 9/23/2019 https://search.bilibili.com
search.bilibili.com##.mobile.nav-item

! 9/23/2019 https://search.jd.com
search.jd.com##.m-aside
search.jd.com###footmark
! 9/23/2019 https://www.jd.com
www.jd.com##.mobile_static
www.jd.com###J_mobile

! 9/24/2019 https://www.taobao.com
www.taobao.com###J_Banner
www.taobao.com##.logo-hover
www.taobao.com##.qr
s.taobao.com###J_shopkeeper_bottom

! 9/24/2019 https://www.tmall.com
www.tmall.com##.j_doodleLink
www.tmall.com##.activity-bg
www.tmall.com###J_FloorCNXH
www.tmall.com###footer
! 9/24/2019 https://chaoshi.tmall.com
chaoshi.tmall.com##.j_MostTop

! 9/24/2019 https://music.163.com
music.163.com##.lst

! 9/24/2019 https://y.qq.com
y.qq.com##.popup_guide
y.qq.com##.top_nav__item--subnav.top_nav__item

! 9/24/2019 https://weibo.com
weibo.com##.clearfix.mini

! 9/24/2019 https://www.newyorker.com
www.newyorker.com##.Layout__sidebar___2GcEE

! 9/24/2019 https://pan.baidu.com
pan.baidu.com###web-single-bottom
pan.baidu.com###web-right-view

! 9/26/2019 https://bbs.nga.cn
bbs.nga.cn###m_cate5 > .w100 > div:nth-of-type(1)
bbs.nga.cn###b_nav > div:nth-of-type(2)
bbs.nga.cn###m_posts_c > span
bbs.nga.cn##.null
bbs.nga.cn##.w100 > span:nth-of-type(1)

! 9/26/2019 https://www.r-bloggers.com
www.r-bloggers.com##.fancybox-skin

! 9/26/2019 https://www.collinsdictionary.com
www.collinsdictionary.com##.qc-cmp-showing.qc-cmp-ui-container

! 9/26/2019 https://dictionary.cambridge.org
dictionary.cambridge.org##.cdo-promo.lbt

! 9/27/2019 https://www.newyorker.com
www.newyorker.com##section.SoloPromoSection__promoWrapper___2zsVH:nth-of-type(3) > .SoloPromoSection__promoColumn___1LgrN
www.newyorker.com##section.SoloPromoSection__promoWrapper___2zsVH:nth-of-type(6) > .SoloPromoSection__promoColumn___1LgrN
www.newyorker.com##.MainHeader__subscribeLink___3TGEc

! 9/27/2019 https://www.nytimes.com
www.nytimes.com##.css-qbefct

! 9/27/2019 https://www.quora.com
www.quora.com###__w2_wzDvxNwr2_view_in_app
www.quora.com###__w2_wdiDvVFS3_view_in_app

! 9/27/2019 https://m.huya.com
m.huya.com##.u-btn-down.header-app-btn
m.huya.com##.pop-download
m.huya.com##.u-btn-down.start-btn

! 9/27/2019 https://www.jiemian.com
www.jiemian.com##.jm-app

! 9/28/2019 http://stream-cr7.net
stream-cr7.net##.navbar-header
stream-cr7.net##.hidden-xs

! 9/29/2019 https://aeon.co
aeon.co##.support-bar
aeon.co##.newsletter-signup--wide.newsletter-signup
aeon.co##.gutter > .newsletter-signup

! 9/30/2019 https://www.rd.com
www.rd.com##.in-content

! 9/30/2019 https://convertio.co
convertio.co##.rate-us

! 9/30/2019 https://web.shanbay.com
web.shanbay.com##.Footer_footerWrap__L4iuD

! 10/2/2019 https://matome.naver.jp
matome.naver.jp###yads_pc_overlay

! 10/2/2019 https://aeon.co
aeon.co##.banner-theme-dark-blue.header-banner
aeon.co##.banner-theme-dark-blue.footer-banner
! 10/4/2019 https://aeon.co
aeon.co##.banner-theme-default.footer-banner
! 10/5/2019 https://aeon.co
aeon.co##.banner-theme-default.header-banner

! 10/5/2019 https://www.reddit.com
www.reddit.com##.mSmall.XPromoAdFeed

! 10/5/2019 https://www.newyorker.com
www.newyorker.com###signup-mainHeaderNewsletter

! 10/5/2019 https://pan.baidu.com
pan.baidu.com##.swal-overlay--show-modal.swal-overlay

! 10/20/2019 https://www.douban.com
www.douban.com###dale_homepage_login_top_right > div
www.douban.com###dale_homepage_login_bottom_right > div

! 10/22/2019 https://m.hupu.com
m.hupu.com###tbhb15636041902100162

! 10/24/2019 http://job.nju.edu.cn
job.nju.edu.cn##.ng-scope > div.ng-scope:nth-of-type(5)

! 10/25/2019 https://medium.com
medium.com##.js-upgradeMembershipAction.u-xs-hide.u-baseColor--buttonNormal.button--withChrome.button--upsellNav.button--small.button
medium.com##.js-upgradeMembershipAction.u-accentColor--buttonDark.button--chromeless.button--dark.button--primary.button

! 10/29/2019 https://m.hupu.com
||shihuo.hupucdn.com/1111/hupu_01.png$image